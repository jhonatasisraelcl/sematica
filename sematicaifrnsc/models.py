# -*- coding: utf-8 -*-
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.utils.http import urlquote
from django.dispatch import receiver
from django.core import validators
from django.utils import timezone
from django.conf import settings
from django.db import models
import datetime #custom
from django.contrib.auth.validators import ASCIIUsernameValidator #custom
from simple_history.models import HistoricalRecords #custom
from django.contrib.auth.views import password_reset, password_reset_confirm #custom
from django.core.urlresolvers import reverse_lazy #custom

class Usuarios(User):
    username_validator = ASCIIUsernameValidator()
    instituicao_ensino = models.CharField(('Instituição de ensino'), max_length=20, blank=False)
    idade = models.IntegerField(('Idade'))
    telefone = models.CharField(('Telefone'), max_length=20, blank=True)
    cpf = models.CharField(('CPF'), max_length=20, blank=True)
    history = HistoricalRecords()

    def __str__ (self):
        return self.username

class CriarUsuario(UserCreationForm):
    class Meta:
        model = Usuarios
        fields = ['username', 'cpf', 'email', 'first_name', 'last_name', 'instituicao_ensino', 'idade', 'telefone']
        # fields = "__all__"
        history = HistoricalRecords()

class Minicurso(models.Model):
    ministrante =  models.ForeignKey(User, on_delete=models.CASCADE)
    titulo = models.CharField(('Títuto'), max_length=50, blank=True)
    data_apresentacao = models.DateTimeField(('Data da apresentação'), editable=True, blank=True)
    data_criacao = models.DateTimeField(('Data de Criação'), editable=False, default=timezone.now)
    status = models.BooleanField(('Aprovado'), default=False)
    history = HistoricalRecords()

    class Meta:
        ordering = ('titulo', 'ministrante', 'data_apresentacao')

    def __str__ (self):
        return str(self.titulo) + ' ' + str(self.ministrante)

'''
python manage.py makemigrations sematicaifrnsc ou python manage.py makemigrations
python manage.py migrate
'''
