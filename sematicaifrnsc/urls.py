# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.shortcuts import get_object_or_404, render
from . import views #ponto (.) significa diretorio atual
from django.core.urlresolvers import reverse
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.contrib.auth import views as auth_views
from django.core.mail import send_mail
from django.contrib.auth.views import (
	login, logout, password_reset, password_reset_done, password_reset_confirm,
	password_reset_complete, password_change, password_change_done
	)

urlpatterns = [
	url(r'^$', views.home, name='home'), #pagina inicial - index
	url(r'^login/$', auth_views.login, {'template_name': 'sematicaifrnsc/login.html'}, name='login'),
	url(r'^logout/$', views.logout_view, name='logout'),

	url(r'^cadastro/$', views.cadastroUsuario, name='cadastro'),
	url(r'^evento/$', views.evento, name='evento'),

	url(r'^minicursos/$', views.minicursos, name='minicursos'),
	url(r'^minicursos/normas$', views.minicursos_cadastro, name='minicursosnormas'),
	url(r'^minicursos/inscricao$', views.minicursos_cadastro, name='minicursosinscricoes'),
	url(r'^minicursos/cadastro$', views.minicursos_cadastro, name='minicursoscadastro'),
	url(r'^minicursos/programacao$', views.minicursos_cadastro, name='minicursosprogramacao'),

	url(r'^trabalhoscientificos/$', views.trabalhoscientificos, name='trabalhoscientificos'),
	url(r'^inscricoes/(?P<user_id>[0-9]+)/$', views.inscricoes, name='inscricoes'),

	url(r'^mudarsenha/$', auth_views.password_change, {'post_change_redirect': password_change_done}, name='mudarsenha'),
	url(r'^mudarsenha/feito$', auth_views.password_change_done,name='mudarsenhafeito'),

	url(r'^resetarsenha/$', auth_views.password_reset, {'template_name': 'sematicaifrnsc/reset_password.html', 'post_reset_redirect': auth_views.password_reset_done, 'email_template_name': 'sematicaifrnsc/reset_password_email.html'}, name='resetarsenha'),
    url(r'^resetarsenha/feito/$', auth_views.password_reset_done, {'template_name': 'sematicaifrnsc/reset_password_done.html'}, name='resetarsenhafeito'),
    url(r'^resetarsenha/confirmacao/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', auth_views.password_reset_confirm, {'template_name': 'sematicaifrnsc/reset_password_confirm.html', 'post_reset_redirect': auth_views.password_reset_complete}, name='resetarsenhaconfirma'),
    url(r'^resetarsenha/completo/$', auth_views.password_reset_complete,{'template_name': 'sematicaifrnsc/reset_password_complete.html'}, name='resetarsenhacompleto'),
]
