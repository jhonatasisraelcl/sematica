from django.contrib import admin
from .models import *
from simple_history.admin import SimpleHistoryAdmin


class UsuariosAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name', 'cpf', 'email', 'idade', 'telefone', 'instituicao_ensino', 'is_active']
    ordering = ['first_name']
    # actions = ['aprovar_minicurso', 'desaprovar_minicurso']


class MinicursoAdmin(admin.ModelAdmin):
    list_display = ['ministrante', 'titulo', 'data_apresentacao', 'data_criacao', 'status',]
    ordering = ['titulo']
    actions = ['aprovar_minicurso', 'desaprovar_minicurso']

    def aprovar_minicurso(self, request, queryset):
        rows_updated = queryset.update(status=True)
        if rows_updated == 1:
            message_bit = "1 minicurso foi"
        else:
            message_bit = "%s minicursos foram" % rows_updated
        self.message_user(request, "%s marcado como aprovado." % message_bit)

    def desaprovar_minicurso(self, request, queryset):
        rows_updated = queryset.update(status=False)
        if rows_updated == 1:
            message_bit = "1 minicurso foi"
        else:
            message_bit = "%s minicursos foram" % rows_updated
        self.message_user(request, "%s marcado como desaprovado." % message_bit)

admin.site.register(Usuarios, UsuariosAdmin)
admin.site.register(Minicurso, MinicursoAdmin)
