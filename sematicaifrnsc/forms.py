from django import forms
from django.core.exceptions import ValidationError

class PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(label=("Email Or Usuario"), max_length=254)
