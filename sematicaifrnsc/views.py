# -*- coding: utf-8 -*-
from django.shortcuts import render #custom
from django.shortcuts import redirect, render_to_response
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse_lazy
from django.views import View
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import Group, Permission #custom
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout #custom
from django.conf import settings
from django.core.urlresolvers import reverse #custom

from .forms import * #custom
from .models import * #custom
from django.core.mail import send_mail #custom
import datetime

#@login_required(login_url="login/")
def home(request):
	return render(request, 'sematicaifrnsc/index.html')

def evento(request):
	return render(request, 'sematicaifrnsc/oevento.html')

@login_required
def inscricoes(request, user_id):
	return HttpResponse('<h1> teste' + str(user_id) + '</h1>')

def minicursos(request):
	minicursos = Minicurso.objects.all()
	return render(request, 'sematicaifrnsc/minicursos.html', {'minicursos': minicursos})

def minicursos_cadastro(request):
	minicursos = Minicurso.objects.all()
	return render(request, 'sematicaifrnsc/minicursoscadastro.html')

def trabalhoscientificos(request):
	return render(request, 'sematicaifrnsc/trabalhoscientificos.html')

def login_view(request):
	return render(request, 'sematicaifrnsc/login.html')

@login_required
def logout_view(request):
	logout(request)
	return redirect('home')

@csrf_protect
def cadastroUsuario(request):
	if request.method == 'POST':
		form = CriarUsuario(request.POST)
		if form.is_valid():
			hora = datetime.datetime.now().time().hour
			user = form.cleaned_data['first_name']
			email = form.cleaned_data['email']
			mensagem = 'Olá, ' + str(user) + '. '
			if hora >= 6 and hora <= 12:
				mensagem += 'Bom dia. '
			elif hora > 12 and hora <= 18:
				mensagem += 'Boa tarde. '
			else:
				mensagem += 'Boa noite. '
			mensagem += 'Seu cadastro na Semana da Matemática IFRN-SC foi realizada com sucesso.'
			form.save()
			send_mail('Cadastro na semana de matemática IFRN-SC', mensagem, 'sematicaifrnsc@gmail.com', [email, 'jailson_donald_cobain@hotmail.com'], fail_silently=False)
			return redirect('home')
	else:
		form = CriarUsuario()
	return render(request, 'sematicaifrnsc/cadastrousuario.html', {'form': form})
